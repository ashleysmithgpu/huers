
static SERVER_STREAMING_PORT: u16 = 2100;

pub mod hue {
    pub mod light {
        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct SWUpdate {
            pub state: String,
            pub lastinstall: String
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct XY {
            pub x: f32,
            pub y: f32
        }

        // See https://rsmck.co.uk/hue
        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct State {
            pub on: bool,
            pub bri: u32,
            pub hue: Option<u32>,
            pub sat: Option<u32>,
            pub effect: Option<String>,
            pub xy: Option<XY>,
            pub ct: u32, // in Mireds https://en.wikipedia.org/wiki/Mired
            pub alert: String,
            pub colormode: String,
            pub mode: String,
            pub reachable: bool
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Startup {
            pub mode: String,
            pub configured: bool
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Config {
            pub archetype: String,
            pub function: String,
            pub direction: String,
            pub startup: Startup
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct CT {
            pub min: u32,
            pub max: u32
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Control {
            pub mindimlevel: u32,
            pub maxlumen: u32,
            pub colorgamuttype: Option<String>,
            pub colorgamut: Option<[[f32;2];3]>,
            pub ct: CT
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Streaming {
            pub renderer: bool,
            pub proxy: bool
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Capabilities {
            pub certified: bool,
            pub control: Control,
            pub streaming: Streaming
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Light {
            pub state: State,
            pub swupdate: SWUpdate,
            pub r#type: String,
            pub name: String,
            pub modelid: String,
            pub manufacturername: String,
            pub productname: String,
            pub capabilities: Capabilities,
            pub config: Config,
            pub uniqueid: String,
            pub swversion: String,
            pub swconfigid: String,
            pub productid: String
        }
    }

    pub mod group {
        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct State {
            pub all_on: bool,
            pub any_on: bool
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Action {
            pub on: bool,
            pub bri: u32,
            pub hue: Option<u32>,
            pub sat: Option<u32>,
            pub effect: Option<String>,
            pub xy: Option<crate::hue::light::XY>,
            pub ct: u32,
            pub alert: String,
            pub colormode: String,
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Stream {
            pub proxymode: String,
            pub proxynode: String,
            pub active: bool,
            pub owner: Option<String>
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub struct Group {
            pub name: String,
            pub lights: std::vec::Vec::<String>,
            pub sensors: std::vec::Vec::<String>,
            pub r#type: String,
            pub state: State,
            pub recycle: bool,
            pub class: String,
            pub action: Action,
            pub stream: Option<Stream>,
            pub locations: Option<std::collections::HashMap<String, (f32,f32,f32)>>
        }
    }

    #[derive(serde::Serialize, Debug, Clone)]
    pub struct SetLightRequest {
        pub on: bool,
        pub bri: u32,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub xy: Option<[f32;2]>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub ct: Option<u32>,
        pub transitiontime: u32
    }

    #[derive(serde::Serialize, Debug, Clone)]
    pub struct SetLightOther {
        #[serde(skip_serializing_if = "Option::is_none")]
        pub alert: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub effect: Option<String>
    }

    #[derive(serde::Serialize, Debug, Clone)]
    pub struct SetStreaming {
        pub active: bool
    }
}

fn kelvin_to_mireds(input: f32) -> f32 {
    1_000_000.0 / input
}

fn mireds_to_kelvin(input: f32) -> f32 {
    1_000_000.0 / input
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
struct ApplicationConfig {
    ip_address: String,
    mac_address: String,
    username: String,
    key: String
}

impl ApplicationConfig {
    fn load() -> Option<ApplicationConfig> {
        if let Some(proj_dirs) = directories::ProjectDirs::from("", "", "huers") {
            let path = proj_dirs.config_dir();
            std::fs::create_dir_all(path).expect("Coudn't create config dir");
            if let Ok(f) = std::fs::File::open(path.join("config.json")) {
                if let Ok(cfg) = serde_json::from_reader(f) {
                    return Some(cfg);
                }
            }
        }
        None
    }
    fn save(&self) {
        if let Some(proj_dirs) = directories::ProjectDirs::from("", "", "huers") {
            let path = proj_dirs.config_dir();
            std::fs::create_dir_all(path).expect("Coudn't create config dir");
            let path2 = path.join("config.json");
            if let Ok(f) = std::fs::File::create(&path2) {
                serde_json::to_writer(f, &self).expect("Failed to save config");
            } else {
                println!("Failed to open config file to save {}", path2.display());
            }
        }
    }
}

fn get_data_from_server<'a, T>(cfg: &ApplicationConfig, path: String) -> Result<T, reqwest::Error>
where
    T: for<'de> serde::Deserialize<'de> {

    let url = "http://".to_owned() + &cfg.ip_address + "/api/" + &cfg.username + "/" + &path;
    println!("GET {}", url);
    let b = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();
    let j = b.get(&url).send()?;
    j.json::<T>()
}

fn put_data_to_server<T>(cfg: &ApplicationConfig, path: String, type_to_serialise: &T) -> Result<(), reqwest::Error>
where
    T: serde::Serialize {

    let url = "http://".to_owned() + &cfg.ip_address + "/api/" + &cfg.username + "/" + &path;
    println!("PUT {}", url);
    let b = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();
    b.put(&url).json(&type_to_serialise).send()?;

    Ok(())
}

fn load_lights(cfg: std::sync::Arc::<std::sync::RwLock::<Option<ApplicationConfig>>>, tx: glib::Sender<Command>) {
    if let Ok(cfg) = cfg.read() {
        if let Some(cfg) = &*cfg {
            match get_data_from_server(&cfg, "lights".to_string()) {
                Ok(lights) => {
                    println!("loaded lights");
                    match get_data_from_server(&cfg, "groups".to_string()) {
                        Ok(groups) => {
                            println!("loaded groups");
                            tx.send(Command::LoadedLights(lights, groups)).expect("Couldn't send data to channel");
                        }
                        Err(e) => {
                            println!("failed loading groups {}", e);
                            tx.send(Command::LoadedLights(lights, None)).expect("Couldn't send data to channel");
                        }
                    }
                }
                Err(e) => {
                    println!("failed loading lights {}", e);
                    tx.send(Command::LoadedLights(None, None)).expect("Couldn't send data to channel");
                }
            }
        }
    }
}

enum ColourMode {
    XY([f32;2]),
    CT(u32)
}

enum Command {
    LoadedLights(Option<std::collections::HashMap<String, hue::light::Light>>, Option<std::collections::HashMap<String, hue::group::Group>>),
    SwitchLight(String, bool),
    SwitchGroup(String, bool),
    SetLight(String, bool, ColourMode, u32)
}

fn build_ui(application: &gtk::Application, cfg: &std::sync::Arc::<std::sync::RwLock::<Option<ApplicationConfig>>>) {

    use gtk::prelude::*;
    let window = gtk::ApplicationWindow::new(application);

    window.set_title("huers");
    window.set_default_size(800, 600);
    window.set_position(gtk::WindowPosition::Center);
    window.set_border_width(10);
    window.set_icon_name(Some("gnome-tweak-tool-symbolic"));

    let model = gio::ListStore::new(row_data::RowData::static_type());
    let left_tree = gtk::ListBox::new();
    let right_box = gtk::Box::new(gtk::Orientation::Vertical, 5);
    right_box.set_sensitive(false);

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    let tx_clone = tx.clone();
    left_tree.bind_model(Some(&model),
        glib::clone!(@weak window => @default-panic, move |item| {
            let tx = tx_clone.clone();
            let box_ = gtk::ListBoxRow::new();
            let item = item.downcast_ref::<row_data::RowData>().expect("Row data is of wrong type");

            let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);

            let label = gtk::LabelBuilder::new().halign(gtk::Align::Start).build();
            item.bind_property("name", &label, "label")
                .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
                .build();
            hbox.pack_start(&label, true, true, 0);

            let switch_button = gtk::Switch::new();
            switch_button.connect_state_set(glib::clone!(@strong item => move |_, state| {
                let tx = tx.clone();
                if item.get_property("type").unwrap().get::<String>().unwrap().unwrap() == "light" {
                    tx.send(Command::SwitchLight(item.get_property("id").unwrap().get::<String>().unwrap().unwrap(), state)).expect("Couldn't send data to channel");
                } else {
                    tx.send(Command::SwitchGroup(item.get_property("id").unwrap().get::<String>().unwrap().unwrap(), state)).expect("Couldn't send data to channel");
                }
                gtk::Inhibit(false)
            }));
            item.bind_property("onoff", &switch_button, "state")
                .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
                .build();
            hbox.pack_start(&switch_button, false, false, 5);


            box_.add(&hbox);

            box_.show_all();

            box_.upcast::<gtk::Widget>()
        }
    ));
    left_tree.set_vexpand(true);

    let cfg_clone = cfg.clone();
    let tx_clone = tx.clone();
    std::thread::spawn(move || load_lights(cfg_clone, tx_clone));

    let top = gtk::Box::new(gtk::Orientation::Vertical, 5);
    let tabs = gtk::Notebook::new();

    tabs.set_sensitive(false);

    let settings_info_bar = gtk::InfoBarBuilder::new().show_close_button(true).message_type(gtk::MessageType::Warning).build();
    let info_bar = gtk::InfoBarBuilder::new().show_close_button(true).message_type(gtk::MessageType::Warning).build();
    info_bar.set_revealed(false);
    info_bar.get_content_area().add(&gtk::Label::with_mnemonic(Some("Couldn't connect to hub, check IP and user settings")));
    info_bar.connect_response(move |x, _| {
        x.set_revealed(false);
    });
    top.add(&info_bar);

    let lights_data = std::sync::Arc::new(std::sync::RwLock::new(std::collections::HashMap::<String, hue::light::Light>::new()));
    let lights_data2 = lights_data.clone();
    let groups_data = std::sync::Arc::new(std::sync::RwLock::new(std::collections::HashMap::<String, hue::group::Group>::new()));
    let groups_data2 = groups_data.clone();
    let cfg_clone = cfg.clone();
    rx.attach(None, glib::clone!(@weak model, @weak right_box, @weak tabs, @weak info_bar => @default-return glib::Continue(true), move |cmd| {
        match cmd {
            Command::LoadedLights(lights, groups) => {

                gio::ListStoreExt::remove_all(&model);

                match lights {
                    Some(lights) => {
                        for l in &lights {
                            // TODO
//                            gio::ListStoreExt::append(&model, &row_data::RowData::new(&l.0, &format!("{}", l.1.name), l.1.state.on));
                        }
                        match groups {
                            Some(groups) => {
                                for g in &groups {
                                    if g.1.r#type == "Room" {
                                        gio::ListStoreExt::append(&model, &row_data::RowData::new(&g.0, "group", &format!("{}", g.1.name), g.1.state.any_on));
                                        for l in &g.1.lights {
                                            if let Some(light) = lights.get(l) {
                                                gio::ListStoreExt::append(&model, &row_data::RowData::new(&l, "light", &format!("    {}", &light.name), light.state.on));
                                            }
                                        }
                                    }
                                }
                                if let Ok(mut data) = groups_data2.write() {
                                    *data = groups.clone();
                                }
                            }
                            None => {
                                println!("no groups");
                            }
                        }
                        if let Ok(mut data) = lights_data2.write() {
                            *data = lights.clone();
                        }
                        tabs.set_sensitive(true);
                    }
                    None => {
                        info_bar.set_revealed(true);
                    }
                }
            },
            Command::SwitchGroup(group, state) => {
                if let Ok(mut ldata) = lights_data2.write() {
                    if let Ok(mut gdata) = groups_data2.write() {
                        if let Some(g) = gdata.get_mut(&group.to_string()) {
                            if g.state.any_on != state {

                                let mut map = std::collections::HashMap::new();
                                map.insert("on", state);
                                if let Ok(cfg) = cfg_clone.read() {
                                    if let Some(cfg) = &*cfg {
                                        println!("SwitchGroup");
                                        put_data_to_server(&cfg, "groups/".to_owned() + &group + "/action", &map).expect("Failed to turn group on");
                                    }
                                }
                                g.state.any_on = state;
                                for lidx in &g.lights {
                                    println!("l {:?}", lidx);
                                    if let Some(l) = ldata.get_mut(&lidx.to_string()) {
                                        println!("setting l {} to {}", l.name, state);
                                        l.state.on = state;
                                        let obj = gio::ListModelExt::get_object(&model, lidx.parse::<u32>().unwrap()).unwrap();
                                        obj.set_property("onoff", &state);
                                    }
                                }
                            }
                        }
                    }
                }
            },
            Command::SwitchLight(light, state) => {
                if let Ok(mut data) = lights_data2.write() {
                    if let Some(l) = data.get_mut(&light.to_string()) {
                        if l.state.on != state {

                            let mut map = std::collections::HashMap::new();
                            map.insert("on", state);
                            if let Ok(cfg) = cfg_clone.read() {
                                if let Some(cfg) = &*cfg {
                                    println!("SwitchLight");
                                    // TODO
                                    put_data_to_server(&cfg, "lights/".to_owned() + &light + "/state", &map).expect("Failed to turn light on");
                                }
                            }
                            l.state.on = state;
                        }
                    }
                }
            },
            Command::SetLight(light, state, cm, brightness) => {
                if let Ok(mut data) = lights_data2.write() {
                    if let Some(l) = data.get_mut(&light.to_string()) {
                        if let ColourMode::XY(xy) = cm {
                            if let Ok(cfg) = cfg_clone.read() {
                                if let Some(cfg) = &*cfg {
                                    let js = hue::SetLightRequest {
                                        on: state,
                                        xy: Some(xy),
                                        ct: None,
                                        bri: brightness,
                                        transitiontime: 0
                                    };
                                    println!("SetLight");
                                    // TODO
                                    put_data_to_server(&cfg, "lights/".to_owned() + &light + "/state", &js).expect("Failed to set light state");
                                }
                            }
                            l.state.xy = Some(hue::light::XY { x: xy[0], y: xy[1] });
                        }
                        if let ColourMode::CT(ct) = cm {
                            if let Ok(cfg) = cfg_clone.read() {
                                if let Some(cfg) = &*cfg {
                                    let js = hue::SetLightRequest {
                                        on: state,
                                        xy: None,
                                        ct: Some(ct),
                                        bri: brightness,
                                        transitiontime: 0
                                    };
                                    println!("ColourMode");
                                    // TODO
                                    put_data_to_server(&cfg, "lights/".to_owned() + &light + "/state", &js).expect("Failed to set light state");
                                }
                            }
                        }
                        l.state.bri = brightness;
                    }
                }
            }
        }

        glib::Continue(true)
    }));

    let container = gtk::HeaderBar::new();
    container.set_title(Some("Huers"));
    container.set_show_close_button(true);

    let assistant = gtk::Assistant::new();
    assistant.set_size_request(400, 300);
    {
        let page0 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        page0.add(&gtk::Label::with_mnemonic(Some("Setup your hub")));
        page0.add(&gtk::Label::with_mnemonic(Some("We will now connect to https://discovery.meethue.com/\n to try to detect the IP of your hub")));
        assistant.append_page(&page0);
        assistant.set_page_title(&page0, "Setup");
        assistant.set_page_type(&page0, gtk::AssistantPageType::Intro);
        assistant.set_page_complete(&page0, true);
    }
    let page1 = gtk::Box::new(gtk::Orientation::Vertical, 5);
    {
        assistant.append_page(&page1);
        page1.add(&gtk::Label::with_mnemonic(Some("Auto detecting hub")));
        let spin = gtk::Spinner::new();
        spin.start();
        page1.add(&spin);
        assistant.set_page_title(&page1, "Auto detect");
        assistant.set_page_type(&page1, gtk::AssistantPageType::Progress);
        assistant.set_page_complete(&page1, false);
    }
    let auto_detect_results = gtk::Label::with_mnemonic(Some("Auto detect failed"));
    {
        let page2 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        assistant.append_page(&page2);
        page2.add(&auto_detect_results);
        assistant.set_page_title(&page2, "Auto detect results");
        assistant.set_page_type(&page2, gtk::AssistantPageType::Progress);
        assistant.set_page_complete(&page2, true);
    }
    let ip = gtk::EntryBuilder::new().placeholder_text("192.168.1.192").build();
    let ip_check_results = gtk::Label::with_mnemonic(Some(""));
    {
        let page3 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        assistant.append_page(&page3);
        page3.add(&gtk::Label::with_mnemonic(Some("IP address of hub")));
        page3.add(&ip);
        let check_button = gtk::Button::with_mnemonic("Check connection");
        check_button.connect_clicked(glib::clone!(@weak ip, @weak ip_check_results => move |_| {
            println!("check IP");
            let ip_address: Result<std::net::IpAddr, _> = std::str::FromStr::from_str(&ip.get_text());
            if let Ok(ip_address) = ip_address {
                let url = "http://".to_owned() + &ip_address.to_string() + "/api";
                println!("GET {}", url);
                let b = reqwest::blocking::Client::builder()
                    .build()
                    .unwrap();
                match b.get(&url).send() {
                    Ok(j) => {
                        let result = j.text();

                        match result {
                            Ok(_) => {
                                println!("Found api");
                                ip_check_results.set_text(&format!("Found HUB at {}", url)[..]);
                            },
                            Err(e) => {
                                println!("Error with check {}", e);
                                ip_check_results.set_text(&format!("Error: {}", e)[..]);
                            }
                        }
                    },
                    Err(e) => {
                        println!("Error with check {}", e);
                        ip_check_results.set_text(&format!("Error: {}", e)[..]);
                    }
                }
            } else {
                println!("Could not decode IP");
            }
        }));
        page3.add(&check_button);
        page3.add(&ip_check_results);
        assistant.set_page_title(&page3, "Hub");
        assistant.set_page_type(&page3, gtk::AssistantPageType::Progress);
        assistant.set_page_complete(&page3, true);
    }
    let user = gtk::Entry::new();
    let psk = gtk::Entry::new();
    {
        let page4 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        assistant.append_page(&page4);
        let rb0 = gtk::RadioButton::with_mnemonic("Create new user");
        page4.add(&rb0);
        let rb1 = gtk::RadioButton::with_mnemonic_from_widget(&rb0, "Use existing user");
        rb1.set_active(false);
        page4.add(&rb1);
        let existing = gtk::Box::new(gtk::Orientation::Vertical, 5);
        existing.add(&gtk::Label::with_mnemonic(Some("User")));
        existing.add(&user);
        existing.add(&gtk::Label::with_mnemonic(Some("PSK")));
        existing.add(&psk);
        existing.set_sensitive(false);
        rb1.connect_toggled(glib::clone!(@weak existing => move |_| {
            if rb0.get_active() {
                existing.set_sensitive(false);
            } else {
                existing.set_sensitive(true);
            }
        }));
        page4.add(&existing);
        assistant.set_page_title(&page4, "User");
        assistant.set_page_type(&page4, gtk::AssistantPageType::Confirm);
        assistant.set_page_complete(&page4, true);
    }
    {
        let page5 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        assistant.append_page(&page5);
        page5.add(&gtk::Label::with_mnemonic(Some("First press the button below")));
        let new_user = gtk::Button::with_mnemonic("Create user");
        new_user.connect_clicked(glib::clone!(@weak assistant, @weak page5 => move |_| {
            println!("create new user");
            assistant.set_page_complete(&page5, true);
        }));
        page5.add(&new_user);
        page5.add(&gtk::Label::with_mnemonic(Some("Then please press the button on your hub")));
        assistant.set_page_title(&page5, "New user");
        assistant.set_page_type(&page5, gtk::AssistantPageType::Progress);
        assistant.set_page_complete(&page5, false);
    }
    {
        let page6 = gtk::Box::new(gtk::Orientation::Vertical, 5);
        assistant.append_page(&page6);
        page6.add(&gtk::Label::with_mnemonic(Some("Done!")));
        assistant.set_page_title(&page6, "Done");
        assistant.set_page_type(&page6, gtk::AssistantPageType::Summary);
        assistant.set_page_complete(&page6, true);
    }

    let cfg_clone = cfg.clone();
    let tx_clone = tx.clone();
    assistant.connect_prepare(glib::clone!(@weak ip, @weak settings_info_bar => move |x, _| {
        if x.get_current_page() == 1 {
            println!("autodetect");

            let url = "https://discovery.meethue.com/".to_string();
            println!("GET {}", url);
            let b = reqwest::blocking::Client::builder()
                .build()
                .unwrap();
            if let Ok(j) = b.get(&url).send() {
                let result = j.json::<std::vec::Vec<std::collections::HashMap::<String, String>>>();

                match result {
                    Ok(j) => {
                        // TODO: handle multiple hubs?
                        if j.len() > 0 && j[0].get("internalipaddress").is_some() {

                            let ip_address_string = j[0].get("internalipaddress").unwrap();
                            let ip_address: Result<std::net::IpAddr, _> = std::str::FromStr::from_str(ip_address_string);
                            if let Ok(ip_address) = ip_address {
                                println!("autodetect ok {:?}", ip_address);
                                auto_detect_results.set_label(&format!("Auto detect succeeded\nHub IP is {}", ip_address)[..]);
                                ip.set_text(&format!("{}", ip_address)[..]);
                            }
                        } else {
                            println!("autodetect error: server returned bad data");
                        }
                    },
                    Err(e) => { println!("autodetect err {:?}", e) }
                }
            }
            x.set_page_complete(&page1, true);
            x.next_page();
        } else if x.get_current_page() == 6 {
            let mut cfg = cfg_clone.write().expect("Failed to get write lock");
            *cfg = Some(ApplicationConfig {
                ip_address: ip.get_buffer().get_text(),
                mac_address: "".to_string(), // TODO
                username: user.get_buffer().get_text(),
                key: psk.get_buffer().get_text()
            });
            (*cfg).as_ref().unwrap().save();

            settings_info_bar.set_revealed(false);

            let cfg = cfg_clone.clone();
            let tx_clone = tx_clone.clone();
            std::thread::spawn(move || load_lights(cfg, tx_clone));
        }
    }));
    assistant.connect_close(move |x| {
        x.hide();
    });
    assistant.connect_cancel(move |x| {
        x.hide();
    });


    let prefs = gtk::MenuButton::new();
    let prefs_icon = gtk::Image::from_icon_name(Some("open-menu-symbolic"), gtk::IconSize::Menu);
    prefs.add(&prefs_icon);

    let menu = gtk::PopoverMenu::new();
    let mbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
    let b = gtk::ModelButtonBuilder::new().label("First time setup").build();
    b.connect_clicked(glib::clone!(@weak assistant, @weak window => move |_| {
        assistant.show_all();
        assistant.present();
        assistant.set_transient_for(Some(&window));
        assistant.set_modal(true);
    }));
    b.show();
    mbox.pack_start(&b, true, true, 5);


    let prefereneces = gtk::ModelButtonBuilder::new().label("Prefereces").build();

    let cfg_clone = cfg.clone();
    let tx_clone = tx.clone();
    prefereneces.connect_clicked(glib::clone!(@weak window, @weak info_bar => move |_| {

        let prefs_dialog = gtk::Dialog::with_buttons(Some("Preferences"), Some(&window), gtk::DialogFlags::MODAL, &[("Save", gtk::ResponseType::Yes), ("Cancel", gtk::ResponseType::No)]);

        prefs_dialog.set_border_width(10);
        prefs_dialog.show_all();

        let b = &prefs_dialog.get_content_area();
        let top = gtk::Grid::new();
        top.set_row_spacing(10);
        top.set_column_spacing(10);
        top.set_border_width(10);
        let ip = gtk::LabelBuilder::new().label("Hub IP address").hexpand(true).halign(gtk::Align::Start).xalign(0.0).justify(gtk::Justification::Left).build();
        top.attach(&ip, 0, 0, 1, 1);
        let ip_entry = gtk::EntryBuilder::new().width_request(100).build();
        top.attach_next_to(&ip_entry, Some(&ip), gtk::PositionType::Right, 1, 1);
        let user = gtk::LabelBuilder::new().label("User").hexpand(true).halign(gtk::Align::Start).xalign(0.0).justify(gtk::Justification::Left).build();
        top.attach(&user, 0, 1, 1, 1);
        let user_entry = gtk::EntryBuilder::new().width_request(400).build();
        top.attach_next_to(&user_entry, Some(&user), gtk::PositionType::Right, 1, 1);
        let psk = gtk::LabelBuilder::new().label("PSK").hexpand(true).halign(gtk::Align::Start).xalign(0.0).justify(gtk::Justification::Left).build();
        top.attach(&psk, 0, 2, 1, 1);
        let psk_entry = gtk::EntryBuilder::new().width_request(400).build();
        top.attach_next_to(&psk_entry, Some(&psk), gtk::PositionType::Right, 1, 1);
        if let Ok(cfg) = cfg_clone.read() {
            if let Some(cfg) = &*cfg {
                ip_entry.set_text(&cfg.ip_address);
                user_entry.set_text(&cfg.username);
                psk_entry.set_text(&cfg.key);
            }
        }
        top.show();
        b.add(&top);
        prefs_dialog.show_all();

        let cfg_clone = cfg_clone.clone();
        let tx_clone = tx_clone.clone();
        prefs_dialog.connect_response(glib::clone!(@weak window, @weak info_bar => move |dialog, response| {
            match response {
                gtk::ResponseType::Yes => {
                    if let Ok(mut cfg) = cfg_clone.write() {
                        *cfg = Some(ApplicationConfig {
                            ip_address: ip_entry.get_text().to_string(),
                            username: user_entry.get_text().to_string(),
                            key: psk_entry.get_text().to_string(),
                            mac_address: "".to_string()
                        });
                        println!("saving");
                        cfg.as_ref().unwrap().save();

                        info_bar.set_revealed(false);

                        let cfg = cfg_clone.clone();
                        let tx_clone = tx_clone.clone();
                        std::thread::spawn(move || load_lights(cfg, tx_clone));
                    }
                }
                _ => {}
            }
            dialog.close();
        }));
    }));
    prefereneces.show();
    mbox.pack_start(&prefereneces, true, true, 5);

    let b = gtk::ModelButtonBuilder::new().label("About Huers").build();
    b.show();

    b.connect_clicked(glib::clone!(@weak window => move |_| {
        let p = gtk::AboutDialog::new();
        p.set_authors(&["Ash"]);
        p.set_website_label(Some("gtk-rs"));
        p.set_website(Some("http://gtk-rs.org"));
        p.set_title("About!");
        p.set_transient_for(Some(&window));
        p.show_all();
    }));

    mbox.pack_start(&b, true, true, 5);
    mbox.show();
    menu.add(&mbox);
    prefs.set_use_popover(true);
    prefs.set_popover(Some(&menu));
    prefs.set_use_popover(true);

    container.pack_end(&prefs);

    // Min max from https://rsmck.co.uk/hue
    let slider_ct = gtk::Scale::with_range(gtk::Orientation::Horizontal, 2000.0, 6493.0, 1.0);
    slider_ct.connect_format_value(|scale, value| {
        let digits = scale.get_digits() as usize;
        format!("{:.*}K", digits, value)
    });
    let slider_x = gtk::Scale::with_range(gtk::Orientation::Horizontal, 0.0, 1.0, 0.001);
    slider_x.connect_format_value(|scale, value| {
        let digits = scale.get_digits() as usize;
        format!("{:.*}", digits, value)
    });
    let slider_y = gtk::Scale::with_range(gtk::Orientation::Horizontal, 0.0, 1.0, 0.001);
    slider_y.connect_format_value(|scale, value| {
        let digits = scale.get_digits() as usize;
        format!("{:.*}", digits, value)
    });
    let slider_brightness = gtk::Scale::with_range(gtk::Orientation::Horizontal, 0.0, 255.0, 1.0);
    slider_brightness.connect_format_value(|scale, value| {
        let digits = scale.get_digits() as usize;
        format!("{:.*}%", digits, (value / 255.0) * 100.0)
    });

    let drawing_area = gtk::DrawingArea::new();
    drawing_area.set_size_request(-1, 100);

    // TODO: unsafe :OO
    unsafe { drawing_area.set_data::<f32>("width", 0.0f32); }
    drawing_area.connect_draw(|drawing_area, cr| {
        let width = unsafe { drawing_area.get_data::<f32>("width").expect("Failed") };
        let gr = cairo::LinearGradient::new(0.0, 0.0, *width as f64, 0.0);
        for x in 0..10 {
            let x = x as f32 / 10.0;
            let k = x * 4400.0 + 2000.0;//x * 4493.0 + 2000.0;
            let rgb = colortemp::temp_to_rgb(k as i64);
            gr.add_color_stop_rgb(x as f64, rgb.r / 255.0, rgb.g / 255.0, rgb.b / 255.0);
        }
        cr.set_source(&gr);
        cr.rectangle(0.0, 0.0, *width as f64, 100.0);
        cr.fill();

        Inhibit(false)
    });
    drawing_area.connect_size_allocate(|w, r| {
        unsafe { w.set_data::<f32>("width", r.width as f32); }
    });

    right_box.add(&drawing_area);
    let l = gtk::Label::with_mnemonic(Some("_Colour Temperature"));
    l.set_mnemonic_widget(Some(&slider_ct));
    right_box.add(&l);
    right_box.add(&slider_ct);
    let l = gtk::Label::with_mnemonic(Some("Chromacity _X"));
    l.set_mnemonic_widget(Some(&slider_x));
    right_box.add(&l);
    right_box.add(&slider_x);
    let l = gtk::Label::with_mnemonic(Some("Chromacity _Y"));
    l.set_mnemonic_widget(Some(&slider_y));
    right_box.add(&l);
    right_box.add(&slider_y);
    let l = gtk::Label::with_mnemonic(Some("_Brightness"));
    l.set_mnemonic_widget(Some(&slider_brightness));
    right_box.add(&l);
    right_box.add(&slider_brightness);
    let set = gtk::Button::with_mnemonic("_Set");
    let flash = gtk::Button::with_mnemonic("_Flash");
    right_box.add(&flash);
    let cfg_clone = cfg.clone();
    flash.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {

        let idx = left_tree.get_selected_row().unwrap().get_index() as u32;
        let obj = gio::ListModelExt::get_object(&model, idx).unwrap();

        let js = hue::SetLightOther {
            alert: Some("select".to_string()),
            effect: Some("none".to_string())
        };
        let id = obj.get_property("id").unwrap().get::<String>().unwrap().unwrap();
        if let Ok(cfg) = cfg_clone.read() {
            if let Some(cfg) = &*cfg {
                put_data_to_server(&cfg, "lights/".to_owned() + &id + "/state", &js).expect("Failed to set state");
            }
        }
    }));
    let colour_loop = gtk::Button::with_mnemonic("_Rainbow");
    right_box.add(&colour_loop);
    let cfg_clone = cfg.clone();
    colour_loop.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {

        let idx = left_tree.get_selected_row().unwrap().get_index() as u32;
        let obj = gio::ListModelExt::get_object(&model, idx).unwrap();

        let js = hue::SetLightOther {
            alert: None,
            effect: Some("colorloop".to_string())
        };
        let id = obj.get_property("id").unwrap().get::<String>().unwrap().unwrap();
        if let Ok(cfg) = cfg_clone.read() {
            if let Some(cfg) = &*cfg {
                put_data_to_server(&cfg, "lights/".to_owned() + &id + "/state", &js).expect("Failed to set state");
            }
        }
    }));
    let tx_clone = tx.clone();
    set.connect_clicked(glib::clone!(@weak model, @weak left_tree, @weak slider_x, @weak slider_y, @weak slider_brightness, @weak slider_ct => move |_| {
        let tx_clone = tx_clone.clone();
        let idx = left_tree.get_selected_row().unwrap().get_index() as u32;
        let obj = gio::ListModelExt::get_object(&model, idx).unwrap();

        let id = obj.get_property("id").unwrap().get::<String>().unwrap().unwrap();
        let state = obj.get_property("onoff").unwrap().get::<bool>().unwrap().unwrap();
        //tx_clone.send(Command::SetLight(id, state, [slider_x.get_value() as f32, slider_y.get_value() as f32], slider_brightness.get_value() as u32)).expect("Couldn't send data to channel");
        tx_clone.send(Command::SetLight(id, state, ColourMode::CT(kelvin_to_mireds(slider_ct.get_value() as f32) as u32), slider_brightness.get_value() as u32)).expect("Couldn't send data to channel");
    }));
    right_box.add(&set);

    let streaming = std::sync::Arc::new(std::sync::atomic::AtomicBool::new(false));

    let streaming_on = gtk::Button::with_mnemonic("on");
    let streaming1 = streaming.clone();
    let cfg_clone = cfg.clone();
    streaming_on.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {

        let streaming2 = streaming1.clone();
        streaming2.swap(true, std::sync::atomic::Ordering::Relaxed);
        let cfg = cfg_clone.clone();
        std::thread::spawn(move || {
            let mut map = std::collections::HashMap::new();
            map.insert("stream", hue::SetStreaming { active: true });
            if let Ok(cfg) = cfg.read() {
                if let Some(cfg) = &*cfg {
                    put_data_to_server(&cfg, "groups/4".to_string(), &map).expect("Failed to turn streaming on");

                    let server_addr = std::net::SocketAddr::new(std::net::IpAddr::V4(std::str::FromStr::from_str(&cfg.ip_address).unwrap()), SERVER_STREAMING_PORT);
                    let client_addr = std::net::SocketAddr::new(std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)), 53341); // TODO: does client port matter?

                    let psk = hex::decode(&cfg.key).unwrap(); // clientkey
                    assert!(psk.len() == 16);
                    let psk_identity = cfg.username.as_bytes(); // username
                    println!("psk {:?}", psk);
                    println!("psk_identity {:?}", psk_identity);
                    let connector = udp_dtls::DtlsConnector::builder()
                        .min_protocol_version(Some(udp_dtls::Protocol::Dtlsv12))
                        .danger_accept_invalid_hostnames(true)
                        .danger_accept_invalid_certs(true)
                        .add_cipher("PSK-AES128-CBC-SHA")
                        .use_sni(false)
                        .identity(udp_dtls::ConnectorIdentity::Psk(udp_dtls::PskIdentity::new(&psk_identity, &psk))).build().unwrap();

                    let client_socket = std::net::UdpSocket::bind(client_addr).unwrap();

                    // Seems like the first "client hello" we send when we connect is ignored
                    // so timeout and try again after 1 second
                    client_socket.set_read_timeout(Some(std::time::Duration::new(1, 0))).expect("Failed to set read timeout");

                    let client_channel = udp_dtls::UdpChannel {
                        socket: client_socket,
                        remote_addr: server_addr,
                    };

                    println!("connecting");
                    if let Ok(mut dtls_client) = connector.connect("", client_channel) {
                        println!("connected");

                        let mut buffer = std::vec::Vec::<u8>::new();
                        buffer.extend_from_slice(&"HueStream".as_bytes());
                        assert!(buffer.len() == 9);
                                                   // version  // seq //reserved //rgb //reserved
                        buffer.extend_from_slice(&[0x01, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00]);
                        assert!(buffer.len() == 16);
                                                   //type //lightid  //r         //g         //b
                        buffer.extend_from_slice(&[0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
                        buffer.extend_from_slice(&[0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
                        buffer.extend_from_slice(&[0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
                        buffer.extend_from_slice(&[0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
                        loop {
                            use std::io::Write;
                            let mut ts = libc::timespec { tv_sec: 0, tv_nsec: 0 };
                            unsafe {
                                libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut ts);
                            }
                            let in_ms = ((ts.tv_sec as u64) * 1000000000 + (ts.tv_nsec as u64)) as f32 / 1000000000.0;

                            for x in 0..4 {
                            let x2 = x as f32 * 1.135711;
                            let r = (in_ms + 0.0 + x2 as f32).sin() as f32 * 0.5 + 0.5;
                            let g = (in_ms + 2.0 + x2 as f32).sin() as f32 * 0.5 + 0.5;
                            let b = (in_ms + 4.0 + x2 as f32).sin() as f32 * 0.5 + 0.5;
                            let ru16 = (r * 256.0) as u8;
                            buffer[16+x*9+3] = ru16;//(ru16 >> 8) as u8;
                            buffer[16+x*9+4] = ru16;//(ru16 & 0xff) as u8;
                            let gu16 = (g * 256.0) as u8;
                            buffer[16+x*9+5] = gu16;//(gu16 >> 8) as u8;
                            buffer[16+x*9+6] = gu16;//(gu16 & 0xff) as u8;
                            let bu16 = (b * 256.0) as u8;
                            buffer[16+x*9+7] = bu16;//(bu16 >> 8) as u8;
                            buffer[16+x*9+8] = bu16;//(bu16 & 0xff) as u8;
                            }
                            dtls_client.write_all(&buffer).expect("Couldnt send");
                            std::thread::sleep(std::time::Duration::from_millis(100));

                            if !streaming2.load(std::sync::atomic::Ordering::Relaxed) {
                                break;
                            }
                        }
                        println!("done streaming");
                    } else {
                        println!("Failed to connect");
                    }
                    }
            }
        });
    }));
    let streaming_off = gtk::Button::with_mnemonic("off");
    let streaming2 = streaming.clone();
    streaming_off.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {
        let streaming3 = streaming2.clone();

        streaming3.swap(false, std::sync::atomic::Ordering::Relaxed);
    }));
    let add_light = gtk::Button::new();
    let add_light_icon = gtk::Image::from_icon_name(Some("list-add-symbolic"), gtk::IconSize::Menu);
    add_light.add(&add_light_icon);
    let remove_light = gtk::Button::new();
    let remove_light_icon = gtk::Image::from_icon_name(Some("list-remove-symbolic"), gtk::IconSize::Menu);
    remove_light.add(&remove_light_icon);
    let move_light_up = gtk::Button::new();

    move_light_up.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {
        let selected = left_tree.get_selected_row();

        if selected != left_tree.get_row_at_index(0) {

            if let Some(selected) = selected {

                let idx = selected.get_index() as u32;
                let previous_idx = idx - 1;

                if left_tree.get_row_at_index(previous_idx as i32).is_some() {

                    let obj = gio::ListModelExt::get_object(&model, idx);
                    gio::ListStoreExt::remove(&model, idx);
                    gio::ListStoreExt::insert(&model, previous_idx, &obj.unwrap());

                    let to_select = left_tree.get_row_at_index(previous_idx as i32);
                    left_tree.select_row(Some(&to_select.unwrap()));
                }
            }
        }
    }));

    let move_light_up_icon = gtk::Image::from_icon_name(Some("up"), gtk::IconSize::Menu);
    move_light_up.add(&move_light_up_icon);
    let move_light_down = gtk::Button::new();
    move_light_down.connect_clicked(glib::clone!(@weak model, @weak left_tree => move |_| {
        let selected = left_tree.get_selected_row();

        if selected != left_tree.get_row_at_index(-1) {

            if let Some(selected) = selected {

                let idx = selected.get_index() as u32;
                let next_idx = idx + 1;

                if left_tree.get_row_at_index(next_idx as i32).is_some() {

                    let obj = gio::ListModelExt::get_object(&model, idx);
                    gio::ListStoreExt::remove(&model, idx);
                    gio::ListStoreExt::insert(&model, next_idx, &obj.unwrap());

                    let to_select = left_tree.get_row_at_index(next_idx as i32);
                    left_tree.select_row(Some(&to_select.unwrap()));
                }
            }
        }
    }));
    let move_light_down_icon = gtk::Image::from_icon_name(Some("down"), gtk::IconSize::Menu);
    move_light_down.add(&move_light_down_icon);
    let action_bar = gtk::ActionBar::new();
    action_bar.add(&add_light);
    action_bar.add(&remove_light);
    action_bar.add(&move_light_up);
    action_bar.add(&move_light_down);
    action_bar.add(&streaming_on);
    action_bar.add(&streaming_off);
    action_bar.set_size_request(200, -1);

    let scrolled_window = gtk::ScrolledWindow::new(gtk::NONE_ADJUSTMENT, gtk::NONE_ADJUSTMENT);
    scrolled_window.add(&left_tree);

    let left_box = gtk::Box::new(gtk::Orientation::Vertical, 5);
    left_box.add(&scrolled_window);
    left_box.add(&action_bar);

    left_tree.connect_selected_rows_changed(glib::clone!(@weak model, @weak right_box, @weak slider_x, @weak slider_y, @weak slider_brightness => move |x| {

        if x.get_selected_rows().len() > 0 {
            right_box.set_sensitive(x.get_selected_rows().len() > 0);

            let idx = x.get_selected_row().unwrap().get_index() as u32;
            let obj = gio::ListModelExt::get_object(&model, idx).unwrap();

            let id = obj.get_property("id").unwrap().get::<String>().unwrap().unwrap();

            slider_x.set_value(0.0);
            slider_y.set_value(0.0);
            slider_ct.set_value(0.0);

            if let Ok(data) = lights_data.read() {
                if let Some(l) = &data.get(&id) {
                    if l.state.colormode == "xy" {
                        let xy = l.state.xy.as_ref().expect("Expected XY color mode");
                        println!("xy mode");
                        slider_x.set_value(xy.x.into());
                        slider_y.set_value(xy.y.into());
                    } else {
                        let mireds = l.state.ct as f64;
                        slider_ct.set_value(mireds_to_kelvin(mireds as f32).into());
                    }
                    slider_brightness.set_value(l.state.bri.into());
                } else {
                    println!("could not find id {}", id);
                }
            } else {
                println!("failed to get read lock for lights_data");
            }
        }
    }));

    let split_pane = gtk::Paned::new(gtk::Orientation::Horizontal);
    split_pane.set_size_request(-1, -1);
    split_pane.add(&left_box);
    split_pane.add(&right_box);

    tabs.add(&split_pane);
    tabs.set_tab_label(&split_pane, Some(&gtk::Label::with_mnemonic(Some("Lights"))));

    let schedules = gtk::Label::with_mnemonic(Some("schedules"));

    tabs.add(&schedules);
    tabs.set_tab_label(&schedules, Some(&gtk::Label::with_mnemonic(Some("Schedules"))));

    settings_info_bar.set_revealed(false);
    if !cfg.read().expect("Couldnt get read lock").is_some() {
        println!("cfg is not some");
        settings_info_bar.get_content_area().add(&gtk::Label::with_mnemonic(Some("No settings file found")));
        let setup = gtk::Button::with_mnemonic("First time setup");
        setup.connect_clicked(glib::clone!(@weak assistant, @weak window => move |_| {
            assistant.show_all();
            assistant.present();
            assistant.set_transient_for(Some(&window));
            assistant.set_modal(true);
        }));
        settings_info_bar.get_content_area().add(&setup);
        settings_info_bar.set_revealed(true);
    }
    settings_info_bar.connect_response(move |x, _| {
        x.set_revealed(false);
    });
    top.add(&settings_info_bar);
    top.add(&tabs);
    window.add(&top);
    window.set_titlebar(Some(&container));
    window.show_all();
}

fn main() {
    let cfg = ApplicationConfig::load();
    println!("{:?}", cfg);
    {
        let cfg_arc = std::sync::Arc::new(std::sync::RwLock::new(cfg.clone()));
        {

            let application = gtk::Application::new(
                Some("com.github.gtk-rs.examples.basic"),
                Default::default(),
            ).expect("failed to initialize GTK application");

            let cfg_clone = std::sync::Arc::clone(&cfg_arc);
            gio::ApplicationExt::connect_activate(&application, move |app| {
                build_ui(app, &cfg_clone);
            });

            let quit = gio::SimpleAction::new("quit", None);
            quit.connect_activate(glib::clone!(@weak application => move |_, _| {
                gio::ApplicationExt::quit(&application);
            }));
            gio::ActionMapExt::add_action(&application, &quit);
            gtk::GtkApplicationExt::set_accels_for_action(&application, "app.quit", &["<Ctrl>Q"]);

            gio::prelude::ApplicationExtManual::run(&application, &[]);
        }
    }
}


mod row_data {
    use glib::subclass;
    use glib::subclass::prelude::*;
    use glib::translate::*;

    // Implementation sub-module of the GObject
    mod imp {
        use super::*;
        use std::cell::RefCell;

        // The actual data structure that stores our values. This is not accessible
        // directly from the outside.
        pub struct RowData {
            name: RefCell<Option<String>>,
            r#type: RefCell<Option<String>>,
            onoff: RefCell<bool>,
            id: RefCell<Option<String>>
        }

        // GObject property definitions for our two values
        static PROPERTIES: [subclass::Property; 4] = [
            subclass::Property("name", |name| {
                glib::ParamSpec::string(
                    name,
                    "Name",
                    "Name",
                    None, // Default value
                    glib::ParamFlags::READWRITE,
                )
            }),
            subclass::Property("onoff", |name| {
                glib::ParamSpec::boolean(
                    name,
                    "OnOff",
                    "OnOff",
                    false,
                    glib::ParamFlags::READWRITE,
                )
            }),
            subclass::Property("type", |r#type| {
                glib::ParamSpec::string(
                    r#type,
                    "Type",
                    "Type",
                    None,
                    glib::ParamFlags::READWRITE,
                )
            }),
            subclass::Property("id", |name| {
                glib::ParamSpec::string(
                    name,
                    "ID",
                    "ID",
                    None,
                    glib::ParamFlags::READWRITE,
                )
            }),
        ];

        // Basic declaration of our type for the GObject type system
        impl ObjectSubclass for RowData {
            const NAME: &'static str = "RowData";
            type ParentType = glib::Object;
            type Instance = subclass::simple::InstanceStruct<Self>;
            type Class = subclass::simple::ClassStruct<Self>;

            glib::glib_object_subclass!();

            // Called exactly once before the first instantiation of an instance. This
            // sets up any type-specific things, in this specific case it installs the
            // properties so that GObject knows about their existence and they can be
            // used on instances of our type
            fn class_init(klass: &mut Self::Class) {
                klass.install_properties(&PROPERTIES);
            }

            // Called once at the very beginning of instantiation of each instance and
            // creates the data structure that contains all our state
            fn new() -> Self {
                Self {
                    name: RefCell::new(None),
                    r#type: RefCell::new(None),
                    onoff: RefCell::new(false),
                    id: RefCell::new(None)
                }
            }
        }

        // The ObjectImpl trait provides the setters/getters for GObject properties.
        // Here we need to provide the values that are internally stored back to the
        // caller, or store whatever new value the caller is providing.
        //
        // This maps between the GObject properties and our internal storage of the
        // corresponding values of the properties.
        impl ObjectImpl for RowData {
            glib::glib_object_impl!();

            fn set_property(&self, _obj: &glib::Object, id: usize, value: &glib::Value) {
                let prop = &PROPERTIES[id];

                match *prop {
                    subclass::Property("name", ..) => {
                        let name = value
                            .get()
                            .expect("type conformity checked by `Object::set_property`");
                        self.name.replace(name);
                    }
                    subclass::Property("type", ..) => {
                        let r#type = value
                            .get()
                            .expect("type conformity checked by `Object::set_property`");
                        self.r#type.replace(r#type);
                    }
                    subclass::Property("onoff", ..) => {
                        let onoff = value
                            .get_some()
                            .expect("type conformity checked by `Object::set_property`");
                        self.onoff.replace(onoff);
                    }
                    subclass::Property("id", ..) => {
                        let id = value
                            .get()
                            .expect("type conformity checked by `Object::set_property`");
                        self.id.replace(id);
                    }
                    _ => unimplemented!(),
                }
            }

            fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
                let prop = &PROPERTIES[id];

                use glib::ToValue;

                match *prop {
                    subclass::Property("name", ..) => Ok(self.name.borrow().to_value()),
                    subclass::Property("type", ..) => Ok(self.r#type.borrow().to_value()),
                    subclass::Property("onoff", ..) => Ok(self.onoff.borrow().to_value()),
                    subclass::Property("id", ..) => Ok(self.id.borrow().to_value()),
                    _ => unimplemented!(),
                }
            }
        }
    }

    // Public part of the RowData type. This behaves like a normal gtk-rs-style GObject
    // binding
    glib::glib_wrapper! {
        pub struct RowData(Object<subclass::simple::InstanceStruct<imp::RowData>, subclass::simple::ClassStruct<imp::RowData>, RowDataClass>);

        match fn {
            get_type => || imp::RowData::get_type().to_glib(),
        }
    }

    // Constructor for new instances. This simply calls glib::Object::new() with
    // initial values for our two properties and then returns the new instance
    impl RowData {
        pub fn new(id: &str, r#type: &str, name: &str, onoff: bool) -> RowData {
            use glib::StaticType;
            use glib::Cast;

            glib::Object::new(Self::static_type(), &[("id", &id), ("type", &r#type), ("name", &name), ("onoff", &onoff)])
                .expect("Failed to create row data")
                .downcast()
                .expect("Created row data is of wrong type")
        }
    }
}

